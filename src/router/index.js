import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import User from '../components/user/User.vue'
Vue.use(VueRouter)
// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const routes = [{
    path: '/Login',
    component: Login
  },
  {
    path: '/',
    redirect: '/Login'
  },
  {
    path: '/home',
    component: Home,
    children: [{
        path: '/welcome',
        component: Welcome
      },
      {
        path: '/users',
        component: User
      }
    ],
    redirect: '/welcome'
  }
]

const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  //to将要访问的路径
  //from 代表从哪个路径跳转而来
  //next 是一个函数 ,表示放行
  //next() 放行  next('/login')  强制跳转
  if (to.path == '/login') return next();
  //获取token
  const tokenStr = window.sessionStorage.getItem('token');
  if (!tokenStr) return next('/login');
  next()
})
export default router